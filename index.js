
function buscador() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("busqueda");
    filter = input.value.toUpperCase();
    table = document.getElementById("buscar");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

document.querySelector('#boton').addEventListener('click', obtenerdatos());


function obtenerdatos() {
    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'datos.json', true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
    
            let datos = JSON.parse(this.responseText);
            console.log(datos);
            let res = document.querySelector('#res');
            res.innerHTML = '';

            
            for (let item of datos) {
                
             
                res.innerHTML += `<tr>
                <td>${item.id}</td>
                <td>${item.apellidos}</td>
                <td>${item.nombres}</td>
                <td>${item.semestre}</td>
                <td>${item.paralelo}</td>
                <td>${item.direccion}</td>
                <td>${item.telefono}</td>
                <td>${item.correo}</td>
                <td>${item.cedula}</td>
                
                </tr>`;
            }
        }
    };
}
